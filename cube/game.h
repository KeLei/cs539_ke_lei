/*
CS539 SDL & OpenGL Rotating cube

Frame code from video: https://www.youtube.com/watch?v=wWGtuc5uqF4&feature=c4-overview-vl&list=PLhXCYa5ehAnGPArNDjwuvoY8i8Dg53k1K
Uses OpenGL library from Angel: http://www.pearsonhighered.com/educator/product/Interactive-Computer-Graphics-A-TopDown-Approach-with-ShaderBased-OpenGL/9780132545235.page

To rotate cube, press 1, 2 or 3.
To stop/start rotation, press space

*/



#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "includes.h"

class game
{
private:
	bool Running;
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Event Event;
	SDL_GLContext gContext;

	//Graphics program
	GLuint gProgramID;
	GLuint gVBO;
	GLuint gVAO;
	GLuint gIBO;
	GLint gVertexPos2DLocation;

public:
	game();

	int OnExecute();
	bool OnInit();
	bool LoadContent();
	void OnEvent( SDL_Event* Event );
	void OnLoop();
	void OnRender();
	void Cleanup();
	bool initGL();

};

//Constructer
game::game()
{
	window = NULL;
	Running = true;

	gProgramID = 0;
	gVertexPos2DLocation = -1;

	gVAO = 0;
	gVBO = 0;
	gIBO = 0;
}

//Execution loop
int game::OnExecute()
{
	if( OnInit() == false )
	{
		return -1;
	}

	while( Running ){
		while( SDL_PollEvent( &Event ) )
		{
			OnEvent( &Event );
		}
		OnLoop();
		OnRender();
		SDL_GL_SwapWindow( window );
	}

	Cleanup();

	return 0;
}

//Render part, set up & display graphics
void game::OnRender()
{

	//Clear color buffer
	glClear( GL_COLOR_BUFFER_BIT );

	//Render cube
	projmat = mat4( 1.0 );
	modelview = mat4( 1.0 );

	//Set up the camera optics
	projmat = projmat* Perspective( 60, 1.0, 0.1, 20.0 ); 

	//Set up model view
	modelview = modelview* LookAt(eye, eye - n, v);
	modelview = modelview* Translate( 0.0, 0.0, -5.0);
	modelview = modelview* RotateX( rotationX );
	modelview = modelview* RotateY( rotationY );
	modelview = modelview* RotateZ( rotationZ );

	//to deal with order of transparent  objects. Restore when done.
	glDepthMask(GL_FALSE); 
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Bind program
	glUseProgram( gProgramID );
	glBindVertexArray( gVAO );

	//Set vertex data
	glBindBuffer( GL_ARRAY_BUFFER, gVBO );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );

	//Enable vertex position
	glEnableVertexAttribArray( gVertexPos2DLocation );

	//End of binding
	glBindVertexArray( 0 );

	//send color and model view to shader
	colorLoc = glGetUniformLocation( gProgramID, "color");
	projLoc       = glGetUniformLocation( gProgramID, "projection");
	modelViewLoc = glGetUniformLocation( gProgramID, "modelview");

	glUniformMatrix4fv(projLoc, 1, GL_TRUE, projmat);
	glUniformMatrix4fv(modelViewLoc, 1, GL_TRUE, modelview);
	glUniform4fv(colorLoc, 1, cyanTansparent);

	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0, 1.0);
	glBindVertexArray( gVAO );
	glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, BUFFER_OFFSET(0) );
	glDisable(GL_POLYGON_OFFSET_FILL);

	//Disable vertex position
	glDisableVertexAttribArray( gVertexPos2DLocation );

	glDepthMask(GL_TRUE);

	//Unbind program
	glUseProgram( NULL );
}

//Deal with events
void game::OnEvent( SDL_Event* Event ){
	if( Event-> type == SDL_QUIT)
		Running = false;

	if( Event->type == SDL_KEYDOWN )
	{
		switch( Event->key.keysym.sym ){

			//Respond to key press:
			//    1 for rotate with x coordinates
			//    2 for rotate with y coordinates
			//    3 for rotate with z coordinates
			//    space for stop/start

		case SDLK_1: toggleX = 1.0; toggleY = 0.0; toggleZ = 0.0; break;
		case SDLK_2: toggleX = 0.0; toggleY = 1.0; toggleZ = 0.0; break;
		case SDLK_3: toggleX = 0.0; toggleY = 0.0; toggleZ = 1.0; break;
		case SDLK_SPACE: if( step == 0.0 )
						 step = 5.0;
					 else
						 step = 0.0;
			break;
		default: break;
		}
	}
}

//Initialize SDL and OpenGL
bool game::OnInit()
{
	if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
		return false;


	//Initialization flag
	bool success = true;

	//Use OpenGL 3.1 core
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

	//Create window
	window = SDL_CreateWindow( "SDL Cube",
		SDL_WINDOWPOS_UNDEFINED, 
		SDL_WINDOWPOS_UNDEFINED, 
		640, 480, 
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

	if( window == NULL )
	{
		printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Create context
		gContext = SDL_GL_CreateContext( window );
		if( gContext == NULL )
		{
			printf( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Initialize GLEW
			glewExperimental = GL_TRUE; 
			GLenum glewError = glewInit();
			if( glewError != GLEW_OK )
			{
				printf( "Error initializing GLEW! %s\n", glewGetErrorString( glewError ) );
			}

			//Use Vsync
			if( SDL_GL_SetSwapInterval( 1 ) < 0 )
			{
				printf( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
			}

			//Initialize OpenGL
			if( !initGL() )
			{
				printf( "Unable to initialize OpenGL!\n" );
				success = false;
			}
		}
	}

	return true;

}

//Initialize OpenGL and vertices data
bool game::initGL()
{
	if( ( gProgramID = InitShader( "vshaderCube.glsl", "fshaderCube.glsl" ) ) <= 0 )
		return false; 

	//Initialize clear color
	glClearColor( 1.f, 1.f, 1.f, 1.f );

	glGenVertexArrays(1, &gVAO);

	//Create VBO & IBO
	glGenBuffers( 1, &gVBO );
	glGenBuffers( 1, &gIBO );

	glUseProgram( gProgramID );
	glBindVertexArray( gVAO );

	glBindBuffer( GL_ARRAY_BUFFER, gVBO );
	glBufferData( GL_ARRAY_BUFFER, sizeof( CubeVertices ), CubeVertices, GL_STATIC_DRAW );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( CubeIndices ), CubeIndices, GL_STATIC_DRAW );

	gVertexPos2DLocation = glGetAttribLocation( gProgramID, "vPosition" );
	glVertexAttribPointer( gVertexPos2DLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL );
	glBindVertexArray( 0 );

	return true;
}

//Deal with data change in graphics
void game::OnLoop()
{
	glViewport(0, 0, 640, 480);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 640, 0.0, 480);

	if ( rotationX < 360.0 )
		rotationX += step * toggleX;
	else
		rotationX = 0;

	if ( rotationY < 360.0 )
		rotationY += step * toggleY;
	else
		rotationY = 0;

	if ( rotationZ < 360.0 )
		rotationZ += step * toggleZ;
	else
		rotationZ = 0;
}

//Clean up when done
void game::Cleanup()
{
	SDL_DestroyWindow( window );
	SDL_Quit();
}

#endif
