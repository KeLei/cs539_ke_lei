//#include "game.h"
//
//game::game()
//{
//	window = NULL;
//	Running = true;
//
//	gProgramID = 0;
//	gVertexPos2DLocation = -1;
//	gVBO = 0;
//	gIBO = 0;
//
//	gRenderQuad = true;
//}
//
//int game::OnExecute()
//{
//	if( OnInit() == false )
//	{
//		return -1;
//	}
//
//	while( Running ){
//		while( SDL_PollEvent( &Event ) )
//		{
//			OnEvent( &Event );
//		}
//		OnLoop();
//		OnRender();
//		SDL_GL_SwapWindow( window );
//	}
//
//	Cleanup();
//
//	return 0;
//}
//
//void game::OnRender()
//{
//
//	//Clear color buffer
//	glClear( GL_COLOR_BUFFER_BIT );
//
//	//Render quad
//	if( gRenderQuad )
//	{
//		//Bind program
//		glUseProgram( gProgramID );
//		glBindVertexArray( gVAO );
//
//		//Enable vertex position
//		glEnableVertexAttribArray( gVertexPos2DLocation );
//
//		//Set vertex data
//		glBindBuffer( GL_ARRAY_BUFFER, gVBO );
//		glVertexAttribPointer( gVertexPos2DLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL );
//
//		//Set index data and render
//		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
//		glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, NULL );
//
//		//Disable vertex position
//		glDisableVertexAttribArray( gVertexPos2DLocation );
//
//		//Unbind program
//		glUseProgram( NULL );
//	}
//}
//
//void game::OnEvent( SDL_Event* Event ){
//    if( Event-> type == SDL_QUIT)
//        Running = false;
//
//	if( Event->type == SDL_KEYDOWN )
//	{
//		if( Event->key.keysym.sym == SDLK_SPACE )
//			gRenderQuad = !gRenderQuad;
//	}
//}
//
//bool game::OnInit()
//{
//	if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
//		return false;
//
//
//	//Initialization flag
//	bool success = true;
//
//	//Use OpenGL 3.1 core
//	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
//	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );
//	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
//
//	//Create window
//	window = SDL_CreateWindow( "SDL Cube",
//		SDL_WINDOWPOS_UNDEFINED, 
//		SDL_WINDOWPOS_UNDEFINED, 
//		640, 480, 
//		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
//
//	if( window == NULL )
//	{
//		printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
//		success = false;
//	}
//	else
//	{
//		//Create context
//		gContext = SDL_GL_CreateContext( window );
//		if( gContext == NULL )
//		{
//			printf( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
//			success = false;
//		}
//		else
//		{
//			//Initialize GLEW
//			glewExperimental = GL_TRUE; 
//			GLenum glewError = glewInit();
//			if( glewError != GLEW_OK )
//			{
//				printf( "Error initializing GLEW! %s\n", glewGetErrorString( glewError ) );
//			}
//
//			//Use Vsync
//			if( SDL_GL_SetSwapInterval( 1 ) < 0 )
//			{
//				printf( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
//			}
//
//			//Initialize OpenGL
//			if( !initGL() )
//			{
//				printf( "Unable to initialize OpenGL!\n" );
//				success = false;
//			}
//		}
//	}
//
//	return true;
//
//}
//
//bool game::initGL()
//{
//	if( gProgramID= InitShader( "vshaderCube.glsl", "fshaderCube.glsl" ) < 0 )
//		return false; 
//
//	//Initialize clear color
//	glClearColor( 1.f, 1.f, 1.f, 1.f );
//
//
//	//Create VBO
//	glGenBuffers( 1, &gVBO );
//	glBindBuffer( GL_ARRAY_BUFFER, gVBO );
//	glBufferData( GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), CubeVertices, GL_STATIC_DRAW );
//
//	//Create IBO
//	glGenBuffers( 1, &gIBO );
//	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
//	glBufferData( GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), CubeIndices, GL_STATIC_DRAW );
//
//
//	gVertexPos2DLocation = glGetAttribLocation( program[0], "vPosition" );
//
//	return true;
//}
//
//
//void game::OnLoop()
//{
//   glViewport(0, 0, 640, 480);
//   glMatrixMode(GL_PROJECTION);
//   glLoadIdentity();
//   gluOrtho2D(0.0, 640, 0.0, 480);
//}
//
//
//void game::Cleanup()
//{
//    SDL_DestroyWindow( window );
//    SDL_Quit();
//}
