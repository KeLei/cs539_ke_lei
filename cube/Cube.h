
#ifndef CUBE_H_INCLUDED
#define CUBE_H_INCLUDED

#define PLANE_NUMBER	4

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;


//Camera
point4 n = vec4(0.0, 0.0, 1.0, 0.0);
point4 v = vec4(0.0, 1.0, 0.0, 0.0);
point4 u = vec4(1.0, 0.0, 0.0, 0.0);
point4 eye = vec4(0.0, 0.0, 0.0, 1.0);

//Rotation
GLfloat step = 5.0;
GLfloat rotationX = 0.0;
GLfloat rotationY = 0.0;
GLfloat rotationZ = 0.0;

bool toggleX;
bool toggleY;
bool toggleZ;


//Cube vertices
point4 CubeVertices[ 8 ] = 
{	
	vec4(  1.0,  1.0,  1.0,  1.0 ),
	vec4(  1.0, -1.0,  1.0,  1.0 ),
	vec4(  1.0,  1.0, -1.0,  1.0 ),
	vec4(  1.0, -1.0, -1.0,  1.0 ),
	vec4( -1.0,  1.0,  1.0,  1.0 ),
	vec4( -1.0, -1.0,  1.0,  1.0 ),
	vec4( -1.0,  1.0, -1.0,  1.0 ),
	vec4( -1.0, -1.0, -1.0,  1.0 ),
};

//Indice of cude
GLubyte CubeIndices[ PLANE_NUMBER * 6 + (PLANE_NUMBER - 2) * 6] = 
{
	0, 1, 2,
	1, 3, 2,

	0, 2, 4,
	2, 6, 4,

	0, 4, 5,
	5, 1, 0,

	4, 6, 7,
	7, 5, 4,

	1, 3, 7,
	1, 7, 5,

	2, 3, 7,
	7, 6, 2,
};


// RGBA olors
color4 cyanTansparent = color4( 0.0, 1.0, 1.0, 2.0 ); 

//uniform variables locations
GLuint colorLoc;
GLuint projLoc;
GLuint modelViewLoc;
GLuint vPosition;

//Camera and view
mat4 projmat = mat4(1.0);
mat4 modelview = mat4(1.0);

#endif