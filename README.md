# 
CS539 SDL & OpenGL Rotating cube

Frame code from video: https://www.youtube.com/watch?v=wWGtuc5uqF4&feature=c4-overview-vl&list=PLhXCYa5ehAnGPArNDjwuvoY8i8Dg53k1K
Uses OpenGL library from Angel: http://www.pearsonhighered.com/educator/product/Interactive-Computer-Graphics-A-TopDown-Approach-with-ShaderBased-OpenGL/9780132545235.page

To rotate cube, press 1, 2 or 3.
To stop/start rotation, press space